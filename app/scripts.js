var domain = window.location.origin
var Http = new XMLHttpRequest()

function send(event) {
    var input = document.getElementById("input")
    var service = "/notification/Notifications"

    var body = {
        "message": input.value,
        "dateTime": getDate()
    }

    sendPost(service, body)
}

function getDate() {
    var date = new Date()
    return date.getFullYear() + "-" + (date.getMonth().toString().length === 1 ? "0" + date.getMonth() : date.getMonth()) + "-" + date.getDate() // 2021-04-12
}

function sendPost(service, body) {
    var reqType = "POST"
    var url = domain + service;
    Http.open(reqType, url);
    Http.setRequestHeader("Content-Type", "application/json")
    Http.send(JSON.stringify(body));

    Http.onreadystatechange = (e) => {
        console.log(Http.responseText)
    }
}
