using { Currency, cuid, managed, sap } from '@sap/cds/common';

namespace js.webpushserver;

@cds.autoexpose
entity Subscriptions : managed {
  key endpoint: String;
  expirationTime: Date;
  p256dh: String;
  auth: String;

  /* key endpoint: String;
  expirationTime: Date;
  keys: {
    p256dh: String;
    auth: String;
  } */
}

@cds.autoexpose
entity Notifications : managed, cuid {
  message: String;
  dateTime: Date;
}

