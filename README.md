# Getting Started

Welcome to your new project.

It contains these folders and files, following our recommended project layout:

| File / Folder  | Purpose                              |
| -------------- | ------------------------------------ |
| `app/`         | content for UI frontends go here     |
| `db/`          | your domain models and data go here  |
| `srv/`         | your service models and code go here |
| `package.json` | project metadata and configuration   |
| `readme.md`    | this getting started guide           |

## Push Server

[https://js-soft.atlassian.net/wiki/spaces/~397093474/blog/2021/03/15/2431811618/Push+Notifications](Confluence Blog)

## SCP Token

https://blogs.sap.com/2020/03/02/using-postman-for-api-testing-with-xsuaa/

## Varpid Keys

Public Key:
BFSTxIx-s7w3P3lmGcglu6cRloXHb0C4PoWtvOW9dtZYIN7jaaIRmsY_SO2PBxHS_2qvkdiZbATSW11vGpNMw3o

Private Key:
DlwJMaWLjXIhW5jIm57_EdXNDJwUY3ZJ5XvUJgdhJxQ

## Learn more

Learn more at https://cap.cloud.sap/docs/get-started/
