
using {js.webpushserver as db} from '../db/schema';

@path : '/notification'

service NotificationService {
    entity Notifications as projection on db.Notifications;

    function sendNotification() returns {
    };
}
