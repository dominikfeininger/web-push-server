
const cds = require('@sap/cds')
const pushService = require("./push-service")

module.exports = async (srv) => {

    srv.after(['CREATE'], 'Notifications', async (data, req) => {
        pushService.sendMessage(data, req)

    })

    // for DB DELETE Testing
    srv.on('sendNotification', async (data, req) => {
        const tmp = {
            dateTime: "",
            message: "some test message !!!"
        }
        pushService.sendMessage(tmnp, req)
        return tmp
    })
}
