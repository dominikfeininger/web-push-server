
using {js.webpushserver as db} from '../db/schema';

@path : '/subscription'

service SubscriptionService {
    entity Subscriptions as projection on db.Subscriptions;

    function getVapidInfos() returns {
        publicKey: String;
        isPrivateKeySet: Boolean;
        subject: String
    };
}
