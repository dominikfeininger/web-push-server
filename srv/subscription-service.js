const cds = require('@sap/cds')
const pushService = require("./push-service")

module.exports = async (srv) => {

  srv.on('getVapidInfos', async (req) => {
    return pushService.getVapidInfos()
  })

};
