const webpush = require('web-push')
const bodyParser = require('body-parser')
require("dotenv").config() // hook up .env to runtime
const { FilterList, serializeEntity } = require('@sap/cloud-sdk-core')
const cds = require('@sap/cds')

const { Subscriptions } = cds.entities("js.webpushserver");

// TODO: read from cred store or env variables
const VAPIDKEYS = {
  privateKey: "DlwJMaWLjXIhW5jIm57_EdXNDJwUY3ZJ5XvUJgdhJxQ", //process.env.PRIVATEKEY,
  publicKey: "BFSTxIx-s7w3P3lmGcglu6cRloXHb0C4PoWtvOW9dtZYIN7jaaIRmsY_SO2PBxHS_2qvkdiZbATSW11vGpNMw3o", //process.env.PUBLICKEY,
  subject: "mailto:testuser@email.com", // process.env.SUBJECT
}

// for DB DELETE Testing
let request = null

// set the keys on service start
//setting our previously generated VAPID keys
webpush.setVapidDetails(
  VAPIDKEYS.subject,
  VAPIDKEYS.publicKey,
  VAPIDKEYS.privateKey
)

module.exports = {

  getVapidInfos: function () {
    return {
      publicKey: VAPIDKEYS.publicKey,
      isPrivateKeySet: VAPIDKEYS.privateKey.length > 0,
      subject: VAPIDKEYS.subject
    }
  },

  sendMessage: async function (dataToSend, req) {

    const subscriptions = await SELECT.from(Subscriptions);
    if (subscriptions.length === 0) {
      console.log("no subscriptions")
    }

    for (let i = 0; i < subscriptions.length - 1; i++) {
      this.triggerPushMsg(this.convertSubscription(subscriptions[i]), dataToSend, req);
    }

  },

  // convert with deep structure of key
  convertSubscription: function (subscription) {
    return {
      endpoint: subscription.endpoint,
      expirationTime: subscription.expirationTime,
      keys: {
        p256dh: subscription.p256dh,
        auth: subscription.auth
      }
    }
  },

  triggerPushMsg: async function (subscription, dataToSend, req) {

    // for DB DELETE Testing
    request = req;

    webpush.sendNotification(subscription, dataToSend.message)
      .then((resp) => {
        if (resp.statusCode) {
          console.log('message sucessfully send with status: ', resp.statusCode);
        } else {
          console.log('something happen ... !!!');
        }
      })
      .catch(async (err) => {
        if (err.statusCode === 404 || err.statusCode === 410) {
          console.log('Subscription has expired or is no longer valid: ', err);

          // for DB DELETE Testing
          // let entity = await SELECT.from(Subscriptions).where({ "endpoint": subscription.endpoint });
          // let code = await cds.run([DELETE.from(Subscriptions).where({ "endpoint": subscription.endpoint })]);
          // const code = await cds.run([DELETE.from(Subscriptions, subscription.endpoint)])
          // let entity = await SELECT.from(Subscriptions);
          // const tx = cds.tx(request)
          // const code = await tx.run(DELETE.from(Subscriptions).where({ endpoint: subscription.endpoint }))
          let code = await DELETE.from(Subscriptions, subscription.endpoint)

          if (code === 1) {
            console.log('deletetd sucessfully from database: ', subscription.endpoint);
          } else {
            console.log('something happend ... !!!');
          }

        } else {
          console.error('something happend ... !!! with Status Code: ' + err.statusCode);
        }
      })

  }
}
