// load modules
const express = require('express')
const cds = require('@sap/cds')
// For authentication test
const passport = require('passport')
const xsenv = require('@sap/xsenv')
xsenv.loadEnv();
const JWTStrategy = require('@sap/xssec').JWTStrategy
const services = xsenv.getServices({ xsuaa: { tags: 'xsuaa' } })
passport.use(new JWTStrategy(services.xsuaa))

var log = require('cf-nodejs-logging-support');

// config
const host = process.env.HOST || '0.0.0.0'
const port = process.env.PORT || 4004

  ; (async () => {
    // create new app
    const app = express()

    // Set the minimum logging level (Levels: off, error, warn, info, verbose, debug, silly)
    log.setLoggingLevel("info");

    // Bind to express app
    app.use(log.logNetwork);

    // Authentication using JWT
    /* await app.use(passport.initialize())
     await app.use(
       passport.authenticate('JWT', { session: false })
     )

     await app.use("/", express.static("resources/"))

     await app.get('/api/userInfo', function (req, res) {
       res.header("Content-Type", "application/json");
       res.send(JSON.stringify(req.user))
     })
     await app.get('/api/jwt', function (req, res) {
       const jwt = /^Bearer (.*)$/.exec(req.headers.authorization)[1]
       res.header("Content-Type", "application/json");
       res.send(JSON.stringify({ "JWT": jwt }))
     })
    */

    // serve odata v4
    // ensure database is connected!
    await cds.connect('db')

    await cds
      .serve('SubscriptionService')
      .from('gen/csn.json')
      .with('subscription-service.js')
      .in(app);

    await cds
      .serve('NotificationService')
      .from('gen/csn.json')
      .with('notification-service.js')
      .in(app);

    // start server
    const server = app.listen(port, host, () => {
      console.info(`app is listing at ${port}`)
      const used = process.memoryUsage().heapUsed / 1024 / 1024;
      console.log(`The script uses approximately ${Math.round(used * 100) / 100} MB`);
    })
    server.on('error', error => console.error(error.stack))
  })()
